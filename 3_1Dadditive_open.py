""" fd1d_1_1.py: 1D FDTD

Simulation in free space
"""
# ამ სიმულაცაში გვაქვს soft source. ეს ნიშნავს რომ არეკლილი ტალღა გაივლის წყაროში და ასევე გვაქვს ღია სივრცე. 
# softsource-ს განაპირობებს წყაროში (ხაზი 48), არსებულ მნიშვნელობაზე დამატება "+=", ez[0] = ez[0] + (რამე)
# ღია არეს განაპირობებს ხაზები 38 და 43. ანუ ბოლო კედლის მნიშვნელობას გადავაწერთ მის წინა მნიშვნელობას.
import numpy as np
import time
from math import exp

from matplotlib import pyplot as plt
# from matplotlib.animation import FuncAnimation

size = 200
source = 100
ez = np.zeros(size)
hy = np.zeros(size)
imp0 = 377.0

qTime = 0
maxTime = 250
mm = 0

# to run GUI event loop
# plt.ion()


fig, ax = plt.subplots(2, 1, figsize=(10, 8))

# setting x-axis label and y-axis label
plt.xlabel("X-axis")
plt.ylabel("Y-axis")



for t in range(qTime, maxTime):
    hy[size - 1] = hy[size - 2]
    # Calculate the Hy field
    for mm in range(size - 1):
        hy[mm] = hy[mm] + (ez[mm + 1] - ez[mm]) / imp0

    ez[0] = ez[1]
        # Calculate the Ez field
    for mm in range(1, size):
        ez[mm] = ez[mm] + (hy[mm] - hy[mm - 1]) * imp0

    ez[source] = ez[source] + exp(-(t - 30) * (t - 30) / 100)

    print(f"qTime = {t} - Ez = {ez[50]}")
    
    ax[0].cla()
    ax[0].plot(ez)
    ax[0].set_xlim([0, size])
    ax[0].set_ylim([-1.2, 1.2])
    
    ax[1].cla()
    ax[1].plot(hy)
    ax[1].set_xlim([0, size])
    ax[1].set_ylim([-0.003, 0.003])

    plt.pause(0.01)


plt.show()
